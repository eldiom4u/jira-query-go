FROM node:lts as nodebuilder

WORKDIR /app
COPY . /app

RUN yarn install && yarn run build-dev

FROM golang:1.16-alpine3.14 AS builder

WORKDIR /app
COPY . /app
RUN apk add build-base git
RUN go test ./... -cover
RUN go build -v -o bin/jira-query -a -ldflags '-extldflags "-static"' . && chmod +x bin/jira-query

FROM alpine:3.14

WORKDIR /app
COPY --from=builder /app/bin/jira-query /app
COPY --chown=0:0 --from=nodebuilder /app/static/js/dist/bundle.js /app/static/js/dist/
COPY templates /app/templates

RUN addgroup -S jiragroup && \
    adduser -S jira -G jiragroup && \
    chown jira. -R /app

EXPOSE 80

USER jira

ENTRYPOINT [ "./jira-query", "server", "-p", "80" ]
