package cmd

import (
	"log"

	"github.com/Eldius/jira-query-go/exporter"
	"github.com/Eldius/jira-query-go/logger"
	"github.com/spf13/cobra"
)

// exportCmd represents the export command
var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "Exports the config queries to a .csv file",
	Long: `Exports the config queries to a .csv file. For example:

jira-query execute -u <user> -p <pass> -f <start-date> -t <end-date> -d <dest-file>

jira-query execute -u myuser -p MyScureP@ass -f 2020-05-01 -t 2020-05-17 -d result_file.csv

`,
	Run: func(cmd *cobra.Command, args []string) {
		queries, err := exporter.ReadQueryFile(args[0])
		if err != nil {
			logger.Logger.Println("Error importing query model file")
			logger.Logger.Panic(err.Error())
		}
		file, err := exporter.CountQuery(queryUser, queryPass, fromDate, toDate, destFile, queries)
		if err != nil {
			logger.Logger.Println("Error executing queires")
			logger.Logger.Panic(err.Error())
		}
		log.Println("Exported file at", file)
	},
}

var (
	fromDate string
	toDate   string
	destFile string
)

func init() {
	rootCmd.AddCommand(exportCmd)

	exportCmd.Flags().StringVarP(&fromDate, "from", "f", "", "--from 2020-05-01 or -f 2020-05-01")
	exportCmd.Flags().StringVarP(&toDate, "to", "t", "", "--to 2020-05-17 or -t 2020-05-17")
	exportCmd.Flags().StringVarP(&destFile, "dest-file", "d", "", "--dest-file my_file.csv or -d my_file.csv")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// exportCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// exportCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
