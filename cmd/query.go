package cmd

import (
	"strings"

	"github.com/Eldius/jira-query-go/config"
	"github.com/Eldius/jira-query-go/jira"
	"github.com/Eldius/jira-query-go/logger"
	"github.com/spf13/cobra"
)

// queryCmd represents the query command
var queryCmd = &cobra.Command{
	Use:   "query",
	Short: "Executes a query passed as parameter",
	Long: `Executes a query passed as parameter
Something like this:

jira-query query -u <user> -p <pass> '<query-jql>'

jira-query query -u myuser -p MyScureP@ass  'project = QA AND label in (toil)'

`,
	Run: func(cmd *cobra.Command, args []string) {
		cfg := config.Load()
		logger.Logger.Println("jira endpoint:", cfg.JiraEndpoint)
		if q, err := jira.Query(strings.Join(args, " "), queryUser, queryPass, cfg.JiraEndpoint); err != nil {
			logger.Logger.Error(err.Error())
		} else {
			logger.Logger.Println(q)
		}
	},
}

func init() {
	rootCmd.AddCommand(queryCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// queryCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// queryCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
