/*
Package cmd has all command definitions
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/Eldius/jira-query-go/logger"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "jira-query-go",
	Short: "A simple tool to extract jira data",
	Long:  `A simple tool to extract jira data.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var (
	queryUser string
	queryPass string
)

func init() {

	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/jira-query-go.yaml)")

	rootCmd.PersistentFlags().StringVarP(&queryUser, "jira-user", "u", "", "Username to log into Jira")
	rootCmd.PersistentFlags().StringVarP(&queryPass, "jira-pass", "W", "", "Password to log into Jira")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("verbose", "v", false, "Verbose log")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			logger.Logger.Error(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".jira-query-go" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		viper.SetConfigName("jira-query-go")
		viper.SetConfigType("yml")
	}

	_ = viper.BindEnv("jiraEndpoint", "QUERY_JIRA_ENDPOINT")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	}
}
