package config

import (
	"log"

	"github.com/spf13/viper"
)

/*
QueryConfig is the execution config
*/
type QueryConfig struct {
	JiraEndpoint string
}

/*
Load loads the app config
*/
func Load() QueryConfig {
	log.Println("Using config file:", viper.ConfigFileUsed())
	var cfg QueryConfig
	err := viper.Unmarshal(&cfg)
	if err != nil {
		panic(err)
	}
	return cfg
}
