package config

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
)

func TestConfigLoad(t *testing.T) {
	cfgFilePath, err := filepath.Abs("./samples/jira-query-go.yml")
	if err != nil {
		t.Error(err)
	}
	i, err := os.Stat(cfgFilePath)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(i.Size())
	log.Println("cfg file:", cfgFilePath)
	viper.SetConfigFile(cfgFilePath)
	viper.AutomaticEnv() // read in environment variables that match
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
	log.Println("cfg file used:", viper.ConfigFileUsed())

	cfg := Load()

	if cfg.JiraEndpoint != "https://m4uservicosdigitais.atlassian.net" {
		t.Errorf("Jira endpoint should be 'https://m4uservicosdigitais.atlassian.net', but was '%s'\n%v\n---", cfg.JiraEndpoint, cfg)
	}
}
