import pytest
import os
import testinfra
import logging
import docker

DOCKER_IMAGE_NAME = 'eldius/jira-query:latest'
NGINX_DOCKER_IMAGE_NAME = 'nginx:alpine'

logger = logging.getLogger('test_configuration')
logger.setLevel(logging.DEBUG)

from selenium import webdriver


def setup_nginx():
    client = docker.from_env()
    print("Starting NGINX container...")
    container = client.containers.run(
        DOCKER_IMAGE_NAME,
        detach=True,
        remove=True,
        volumes={f"{os.getcwd()}/api_responses": {"bind": "/usr/share/nginx/html/rest/api/2", "mode": "ro"}}
    )

    print(f"nginx container.id: {container.id}")
    nginx = testinfra.get_host(f"docker://{container.id}")
    return container, nginx


@pytest.fixture(scope='class')
def host(request):
    nginx_container, nginx = setup_nginx()
    client = docker.from_env()
    print("Starting Docker container...")
    container = client.containers.run(
        DOCKER_IMAGE_NAME,
        detach=True,
        remove=True,
        environment=[f"QUERY_JIRA_ENDPOINT=http://{nginx.interface('eth0').addresses[0]}"],
    )

    print(f"container.id: {container.id}")
    host = testinfra.get_host(f"docker://{container.id}")
    request.cls.host = host
    yield host
    print(f"Removing container: {container.id}")
    container.stop()
    nginx_container.stop()

@pytest.fixture(scope='function')
def driver(request):
    print(" => SETUP driver ")

    try:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        driver = webdriver.Chrome(options=chrome_options)
        driver.implicitly_wait(10)

        driver.implicitly_wait(5)
        driver.set_window_size(1920, 1040)

        request.cls.driver = driver

        yield driver

        print(" => TEARDOWN driver ")
        driver.quit()
    except Exception as e:
        print("Failed to setup webdriver")
        print(e)
        raise e
