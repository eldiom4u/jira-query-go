from unittest import TestCase
import pytest
import requests


@pytest.mark.usefixtures("host")
class TestRequirements(TestCase):

    def setUp(self):
        super(TestRequirements, self).setUp()

    def get_base_endpoint(self):
        return f"http://{self.host.interface('eth0').addresses[0]}"

    def test_app_files(self):
        print("test_app_files")
        env_file = self.host.file('/app/jira-query')
        env_file.exists

    def test_app_html_template(self):
        print("test_app_html_template")
        index_file = self.host.file('/app/templates/index.html')
        index_file.exists
        header_file = self.host.file('/app/templates/_header.html')
        header_file.exists

    def test_process_running(self):
        print("test_process_running")
        processes = self.host.process.filter(user="jira", comm="jira-query")
        for p in processes:
            print(p.args)
        assert processes[0].args == './jira-query server -p 80'
        assert len(processes) == 1

    def test_app_is_listening_in_port_80(self):
        print("test_app_is_listening_in_port_80")
        assert self.host.socket("tcp://0.0.0.0:80").is_listening

    def test_index_page_is_ok(self):
        print("test_index_page_is_ok")
        r = requests.get(f"{self.get_base_endpoint()}/")
        assert r.status_code == 200

    def test_main_js_is_ok(self):
        print("test_main_js_is_ok")
        r = requests.get(f"{self.get_base_endpoint()}/static/js/dist/bundle.js")
        assert r.status_code == 200
