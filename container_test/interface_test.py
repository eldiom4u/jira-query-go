from unittest import TestCase
from pathlib import Path
import pytest
import os

from selenium.webdriver.common.by import By
#from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import time

WEBDRIVER_WAIT_TIME_IN_SECONDS=10
@pytest.mark.usefixtures("host", "driver")
class TestInterfaceRequirements(TestCase):

    def setUp(self):
        super(TestInterfaceRequirements, self).setUp()
        self.driver.get(self.get_base_endpoint())


    def screenshot(self, name):
        full_name = os.environ.get('PYTEST_CURRENT_TEST').split(' ')[0]
        #print(f"full_name: {full_name}")
        test_file_name = full_name.split("::")[0].split('/')[-1].split('.py')[0]
        test_class_name = full_name.split("::")[1]
        test_method_name = full_name.split("::")[2]
        dest_folder = f"{os.getcwd()}/screensoths/{test_file_name}/{test_class_name}/{test_method_name}"
        #print(f"creating folder: {dest_folder}")
        Path(dest_folder).mkdir(parents=True, exist_ok=True)
        self.driver.save_screenshot(f"{dest_folder}/{name}.png")


    def get_base_endpoint(self):
        return f"http://{self.host.interface('eth0').addresses[0]}"


    def test_index_page(self):
        self.screenshot("test_index_page_main")
        assert 'Jira Query Counter' == self.driver.title


    def click_execute(self):
        WebDriverWait(self.driver, WEBDRIVER_WAIT_TIME_IN_SECONDS).until(expected_conditions.presence_of_element_located((By.ID, "executeQuery")))
        self.driver.find_element(By.ID, "executeQuery").click()


    def test_execute_wo_file(self):
        self.screenshot("01-test_execute_wo_file")
        self.click_execute()
        self.screenshot("02-test_execute_wo_file")

        elements = self.driver.find_elements(By.CSS_SELECTOR, ".text-center")
        assert len(elements) == 1

        try:
            while self.driver.find_element(By.CSS_SELECTOR, ".text-center").is_displayed():
                print("waiting...")
        except Exception as e:
            pass

        WebDriverWait(self.driver, WEBDRIVER_WAIT_TIME_IN_SECONDS).until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, "li.list-group-item")))

        time.sleep(3)
        elements = self.driver.find_elements_by_css_selector("li.list-group-item")

        print(f"len(elements): {len(elements)}")

        self.screenshot("03-test_execute_wo_file")

        assert len(elements) > 0
        assert self.driver.find_element(By.CSS_SELECTOR, "li.list-group-item").text == "Failed to execute query file: No file found"

        self.screenshot("04-test_execute_wo_file")
