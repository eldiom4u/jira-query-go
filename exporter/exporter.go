package exporter

import (
	"encoding/csv"
	"os"
	"path/filepath"
	"strconv"

	"github.com/Eldius/jira-query-go/config"
	"github.com/Eldius/jira-query-go/jira"
	"github.com/Eldius/jira-query-go/logger"
	"github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
)

func prepareFile(destFile string) (file *os.File, absFilePath string, err error) {
	absFilePath, err = homedir.Expand(destFile)
	if err != nil {
		return
	}
	destFolder := filepath.Dir(absFilePath)
	_ = os.MkdirAll(destFolder, os.ModePerm)
	file, err = os.Create(absFilePath)
	if err != nil {
		return
	}
	return
}

/*
CountQuery executes all queries and returns only the count of results
to a CSV file
*/
func CountQuery(user string, pass string, fromDate string, toDate string, destFile string, queriesMap QueryesMap) (absFilePath string, err error) {

	var file *os.File
	file, absFilePath, err = prepareFile(destFile)
	if err != nil {
		return
	}
	defer file.Close()

	w := csv.NewWriter(file)
	w.Comma = ';'
	defer w.Flush()

	_ = w.Write([]string{"query name", "parameters group name", "count", "query"})

	cfg := config.Load()
	for k, q := range queriesMap.Queries {
		logger.Logger.Println(k, ":", q)
		var queryMap map[string]string
		queryMap, err = q.ParseQuery(fromDate, toDate)
		if err != nil {
			return
		}
		for n, query := range queryMap {
			var count int
			count, err = countQueryResults(user, pass, query, cfg)
			if err != nil {
				return
			}
			_ = w.Write([]string{k, n, strconv.Itoa(count), query})
		}
	}

	return
}

func countQueryResults(user string, pass string, query string, cfg config.QueryConfig) (qtd int, err error) {
	qr, err := jira.QueryAPI(query, user, pass, 0, 0, []string{"key"}, cfg.JiraEndpoint)
	if err != nil {
		return
	}
	logger.Logger.Println(" => total:", qr.Total)
	qtd = qr.Total
	logger.Logger.WithFields(logrus.Fields{
		"query":        query,
		"totalResults": qtd,
	}).Info("countQueryResults")
	return
}
