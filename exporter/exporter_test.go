package exporter

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/Eldius/jira-query-go/config"
	"github.com/spf13/viper"
)

func setupJira() *httptest.Server {

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, _ := ioutil.ReadAll(r.Body)
			if strings.Contains(string(body), "fields") {
				responseBody, _ := ioutil.ReadFile("./samples/jira_responses/jira_query_fields.json")
				_, _ = w.Write(responseBody)
			} else {
				responseBody, _ := ioutil.ReadFile("./samples/jira_responses/jira_query_all.json")
				_, _ = w.Write(responseBody)
			}
		}),
	)
	//ts.Start()

	viper.SetDefault("jiraEndpoint", ts.URL)

	return ts
}

func createQueryesMap(q QueryDef) QueryesMap {
	return QueryesMap{
		Queries: map[string]QueryDef{
			"default": q,
		},
	}
}

func createQueryWithKeys() QueryDef {
	return QueryDef{
		Query:  "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = {{ index . \"Squad\" }} AND status = Done AND resolved >= {{ index . \"StartDate\" }} AND resolved <= {{ index . \"EndDate\" }} order by lastViewed DESC",
		Fields: []string{"key"},
		Params: map[string]map[string]string{
			"adesao1": {
				"Squad": "Adesao1",
			},
			"adesao2": {
				"Squad": "Adesao2",
			},
		},
	}
}

func TestCount(t *testing.T) {
	ts := setupJira()
	defer ts.Close()

	resultFile, err := ioutil.TempFile("", "query-result*.csv")
	if err != nil {
		t.Error(err)
	}
	t.Log("tmp file:", resultFile.Name())
	absFile, err := CountQuery("user", "pass", "2020-05-12", "2020-06-26", resultFile.Name(), createQueryesMap(createQueryWithKeys()))
	if err != nil {
		t.Error(err)
	}
	if resultFile.Name() != absFile {
		t.Errorf("result file not equals to created temp file tmp:'%s' != result:'%s'", resultFile.Name(), absFile)
	}
}

func TestCountQueryResults(t *testing.T) {
	ts := setupJira()
	defer ts.Close()

	q := createQueryDefWithoutParams()
	cfg := config.Load()

	jql, _ := q.ParseQuery("2020-05-12", "2020-06-26")
	qtd, err := countQueryResults("user", "pass", jql["main"], cfg)
	if err != nil {
		t.Error(err)
	}

	if qtd != 15 {
		t.Errorf("countQuery should have 15 issues, but returned '%d'", qtd)
	}
}
