package exporter

import (
	"bytes"
	"io/ioutil"
	"text/template"

	"github.com/Eldius/jira-query-go/logger"
	"github.com/mitchellh/go-homedir"
	"gopkg.in/yaml.v2"
)

/*
QueryDef represents the query derfinition data
*/
type QueryDef struct {
	Query  string                       `yaml:"query"`
	Fields []string                     `yaml:"fields"`
	Params map[string]map[string]string `yaml:"params"`
}

/*
QueryesMap represents a query mao
*/
type QueryesMap struct {
	Queries map[string]QueryDef `yaml:"queries"`
}

/*
ParseQuery parses the queries
*/
func (q *QueryDef) ParseQuery(fromDate string, toDate string) (queryMap map[string]string, err error) {
	queryMap = make(map[string]string)

	var t *template.Template
	t, err = template.New("todos").Parse(q.Query)

	if len(q.Params) > 0 {
		if err != nil {
			return
		}

		for n, params := range q.Params {
			params["EndDate"] = toDate
			params["StartDate"] = fromDate
			var jql bytes.Buffer

			err = t.Execute(&jql, params)
			if err != nil {
				return
			}
			queryMap[n] = jql.String()
		}

	} else {
		params := map[string]string{
			"EndDate":   toDate,
			"StartDate": fromDate,
		}
		var jql bytes.Buffer

		err = t.Execute(&jql, params)
		if err != nil {
			return
		}
		queryMap["main"] = jql.String()
	}
	return
}

/*
ReadQueryFile parse the query file
*/
func ReadQueryFile(queryFile string) (queries QueryesMap, err error) {
	queryFile, err = homedir.Expand(queryFile)
	if err != nil {
		return
	}

	txt, err := ioutil.ReadFile(queryFile)
	if err != nil {
		return
	}

	if err != nil {
		logger.Logger.Println("Failed to load query file")
		logger.Logger.Error(err.Error())
		return
	}
	_ = yaml.Unmarshal(txt, &queries)
	return
}
