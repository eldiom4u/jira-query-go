package exporter

import (
	"path/filepath"
	"testing"
	"log"
)

func createQueryDefWithParams() QueryDef {
	return QueryDef{
		Query:  "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = {{ index . \"Squad\" }} AND status = Done AND resolved >= {{ index . \"StartDate\" }} AND resolved <= {{ index . \"EndDate\" }} order by lastViewed DESC",
		Fields: []string{"key"},
		Params: map[string]map[string]string{
			"adesao1": {
				"Squad": "Adesao1",
			},
			"adesao2": {
				"Squad": "Adesao2",
			},
		},
	}
}
func createQueryDefWithoutParams() QueryDef {
	return QueryDef{
		Query:  "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = {{ index . \"Squad\" }} AND status = Done AND resolved >= {{ index . \"StartDate\" }} AND resolved <= {{ index . \"EndDate\" }} order by lastViewed DESC",
		Fields: []string{"key"},
		Params: map[string]map[string]string{},
	}
}

func TestConfigLoadWithQueryParams(t *testing.T) {
	queryFilePath, err := filepath.Abs("./samples/single-query-parameterized.yml")
	if err != nil {
		t.Error(err)
	}

	queryMap, err := ReadQueryFile(queryFilePath)
	if err != nil {
		t.Error(err)
	}

	if len(queryMap.Queries) != 1 {
		t.Errorf("Should have 2 queries, but has %d", len(queryMap.Queries))
	}

	if queryMap.Queries["vazao"].Params["adesao1"]["Squad"] != "Adesao1" {
		t.Errorf("queryMap.Queries[\"vazao adesao\"].Params[\"adesao1\"][\"squad\"] must be equals to \"Adesao1\", but was '%s'", queryMap.Queries["vazao adesao"].Params["adesao1"]["Squad"])
	}
	if queryMap.Queries["vazao"].Params["adesao2"]["Squad"] != "Adesao2" {
		t.Errorf("queryMap.Queries[\"vazao adesao\"].Params[\"adesao1\"][\"squad\"] must be equals to \"Adesao1\", but was '%s'", queryMap.Queries["vazao adesao"].Params["adesao1"]["Squad"])
	}
}

func TestConfigLoadWithoutQueryParams(t *testing.T) {
	queryFilePath, err := filepath.Abs("./samples/single-query.yml")
	if err != nil {
		t.Error(err)
	}

	queryMap, err := ReadQueryFile(queryFilePath)
	if err != nil {
		t.Error(err)
	}

	if len(queryMap.Queries) != 1 {
		t.Errorf("Should have 2 queries, but has %d", len(queryMap.Queries))
	}

	if len(queryMap.Queries["vazao adesao"].Params) > 0 {
		t.Errorf("We should not have params, but we have '%d'", len(queryMap.Queries["vazao adesao"].Params))
	}
	if queryMap.Queries["vazao adesao"].Query != "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = Adesao1 AND status = Done AND resolved >= {{ index . \"StartDate\" }} AND resolved <= {{ index . \"EndDate\" }} order by lastViewed DESC" {
		t.Errorf("Wrong query value: '%s'", queryMap.Queries["vazao adesao"].Query)
	}
}

func TestQueryParseWithParams(t *testing.T) {
	q := createQueryDefWithParams()

	queryMap, err := q.ParseQuery("2020-05-12", "2020-06-26")
	if err != nil {
		t.Error("Error parsing a valid query object", err.Error())
	}

	if len(queryMap) != 2 {
		t.Errorf("Should return 2 queries, but returned '%d'", len(queryMap))
	}

	t.Log(q.Query)
	parsedQueryAdesao1 := "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = Adesao1 AND status = Done AND resolved >= 2020-05-12 AND resolved <= 2020-06-26 order by lastViewed DESC"
	if queryMap["adesao1"] != parsedQueryAdesao1 {
		t.Errorf("Invalid parsed query for adesao1 '%s' != '%s'", parsedQueryAdesao1, queryMap["adesao1"])
	}
	parsedQueryAdesao2 := "issuetype in (\"Bug Production\", Compliance, \"New Feature\", \"Support Production\") AND project = OICONTROLE AND labels = Adesao2 AND status = Done AND resolved >= 2020-05-12 AND resolved <= 2020-06-26 order by lastViewed DESC"
	if queryMap["adesao2"] != parsedQueryAdesao2 {
		t.Errorf("Invalid parsed query for adesao2 '%s' != '%s'", parsedQueryAdesao2, queryMap["adesao2"])
	}
}

func TestQueryParser02(t *testing.T)  {
	q := QueryDef{
		Query: "project = OICONTROLE AND labels = tech AND labels in (\"{{ index . \"Squad\" }}\", \"{{ index . \"squad\" }}\") AND status = Done AND resolved >= \"{{ .StartDate }}\" AND resolved <= \"{{ .EndDate }}\" ORDER BY lastViewed DESC",
		Fields: []string{"key"},
		Params: map[string]map[string]string{
			"recorrencia": {
				"Squad": "Recorrencia",
				"squad": "recorrencia",
			},
		},
	}

	qm, err := q.ParseQuery("2020-06-15", "2020-06-28")
	if err != nil {
		t.Error(err)
	}
	log.Println(qm)
	if qm["recorrencia"] != "project = OICONTROLE AND labels = tech AND labels in (\"Recorrencia\", \"recorrencia\") AND status = Done AND resolved >= \"2020-06-15\" AND resolved <= \"2020-06-28\" ORDER BY lastViewed DESC" {
		t.Errorf("'project = OICONTROLE AND labels = tech AND labels in (\"Recorrencia\", \"recorrencia\") AND status = Done AND resolved >= \"2020-06-15\" AND resolved <= \"2020-06-28\" ORDER BY lastViewed DESC' != '%s'", qm["recorrencia"])
	}
}
