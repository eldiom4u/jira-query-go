module github.com/Eldius/jira-query-go

go 1.14

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	gopkg.in/yaml.v2 v2.3.0
)
