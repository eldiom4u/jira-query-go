package jira

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/Eldius/jira-query-go/logger"
	"github.com/sirupsen/logrus"
)

/*
Query executes a query into the API
(still making requests until receive all data)
*/
func Query(jpl string, user string, pass string, host string) (issues []Issue, err error) {

	fields := []string{"id", "key", "labels"}
	fetchCount := 0

	q, err := QueryAPI(jpl, user, pass, fetchCount, 100, fields, host)
	if err != nil {
		return
	}

	for fetchCount < q.Total {
		issues = append(issues, q.Issues...)
		fetchCount = len(issues)
		q, err = QueryAPI(jpl, user, pass, fetchCount, 100, fields, host)
		if err != nil {
			return
		}
	}
	logger.Logger.Println("total issues:", len(issues))
	return
}

/*
QueryAPI executes a direct query to the API
*/
func QueryAPI(jql string, user string, pass string, startAt int, maxResults int, fields []string, host string) (q QueryResponse, err error) {
	urlBase, err := url.Parse(fmt.Sprintf("%s/rest/api/2/search", host))
	if err != nil {
		logger.Logger.Error(err.Error())
		return
	}

	reqMap := map[string]interface{}{
		"jql":        jql,
		"maxResults": maxResults,
		"startAt":    startAt,
		"fields":     fields,
	}
	reqBody, _ := json.Marshal(reqMap)

	log := logger.Logger.WithFields(logrus.Fields{
		"body":     reqBody,
		"endpoint": urlBase,
		"jql":      jql,
	})

	client := &http.Client{}
	req, _ := http.NewRequest("POST", urlBase.String(), bytes.NewBuffer(reqBody))
	req.SetBasicAuth(user, pass)
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		log.Error("Failed to query Jira API", err)
	}
	defer res.Body.Close()

	_ = json.NewDecoder(res.Body).Decode(&q)

	log.WithFields(logrus.Fields{
		"jiraStatusCode": res.Status,
		"issues":         len(q.Issues),
	}).Info("jiraQueryExecution")

	return
}
