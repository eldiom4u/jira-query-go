package jira

/*
QueryResponse represents a query
response
*/
type QueryResponse struct {
	Expand     string  `json:"expand"`
	StartAt    int     `json:"startAt"`
	MaxResults int     `json:"maxResults"`
	Total      int     `json:"total"`
	Issues     []Issue `json:"issues"`
}

/*
Issue represents the issue itself
*/
type Issue struct {
	Expand string `json:"expand"`
	ID     string `json:"id"`
	Self   string `json:"self"`
	Key    string `json:"key"`
	Fields Field  `json:"fields"`
}

/*
Field issue fields
*/
type Field struct {
	Labels []string `json:"labels"`
}
