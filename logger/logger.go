package logger

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func init() {

	hostname, _ := os.Hostname()
	var standardFields = log.Fields{
		"hostname": hostname,
		"appname":  "jira-query",
	}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetReportCaller(true)
	Logger = log.WithFields(standardFields)
}

/*
Logger default system log
*/
var Logger *log.Entry
