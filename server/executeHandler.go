package server

import (
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/Eldius/jira-query-go/exporter"
	"github.com/Eldius/jira-query-go/logger"
	"github.com/sirupsen/logrus"
)

// ExecuteHandler is the handler for query execution
func ExecuteHandler(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseMultipartForm(10 << 20)
	file, _, err := r.FormFile("queryFile")
	if err != nil {
		handleError("No file found", 400, w, r, err)
		return
	}
	defer file.Close()

	tempFile, err := ioutil.TempFile("", "query-*.yml")
	if err != nil {
		handleError("Failed to create temp query file", 500, w, r, err)
		return
	}
	defer tempFile.Close()

	tempResultFile, err := ioutil.TempFile("", "query-result*.csv")
	if err != nil {
		handleError("Failed to create temp result file", 500, w, r, err)
		return
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		handleError("Failed to read query file", 500, w, r, err)
		return
	}
	_, _ = tempFile.Write(fileBytes)

	fromDate := r.FormValue("fromDate")
	toDate := r.FormValue("toDate")
	jiraMail := r.FormValue("mail")
	jiraToken := r.FormValue("token")

	if fromDate == "" {
		msg := "Start date is empty"
		handleError(msg, 400, w, r, errors.New(msg))
		return
	}

	if toDate == "" {
		msg := "End date is empty"
		handleError(msg, 400, w, r, errors.New(msg))
		return
	}

	if jiraMail == "" {
		msg := "Jira mail is empty"
		handleError(msg, 400, w, r, errors.New(msg))
		return
	}

	if jiraToken == "" {
		msg := "Jira token is empty"
		handleError(msg, 400, w, r, errors.New(msg))
		return
	}

	q, err := exporter.ReadQueryFile(tempFile.Name())
	if err != nil {
		handleError("Failed to parse query file", 400, w, r, err)
		return
	}

	responseFile, err := exporter.CountQuery(jiraMail, jiraToken, fromDate, toDate, tempResultFile.Name(), q)
	if err != nil {
		handleError("Failed to export result file", 500, w, r, err)
		return
	}

	response, err := ioutil.ReadFile(responseFile)
	if err != nil {
		handleError("Failed to read result file", 500, w, r, err)
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename=result_export.csv")
	w.Header().Set("Content-Type", "application/csv")
	_, _ = w.Write(response)
}

func handleError(msg string, statusCode int, w http.ResponseWriter, r *http.Request, err error) {
	logger.Logger.WithError(err).
		WithFields(logrus.Fields{
			"statusCode": statusCode,
			"requestUri": r.URL.String(),
		}).
		Error(msg)
	w.WriteHeader(statusCode)
	_, _ = w.Write([]byte(msg))
}
