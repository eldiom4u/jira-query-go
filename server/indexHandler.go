package server

import (
	"html/template"
	"net/http"
)

/*
IndexModel index page model
*/
type IndexModel struct {
	ErrorMsg string
}

var temp = template.Must(template.ParseGlob("templates/*.html"))

// IndexHandler is the handler for index path
func IndexHandler(w http.ResponseWriter, _ *http.Request) {
	_ = temp.ExecuteTemplate(w, "Index", IndexModel{
		ErrorMsg: "",
	})
}
