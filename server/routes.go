/*
Package server has all the webapp
related things
*/
package server

import (
	"fmt"
	"net/http"
)

/*
Serve starts server
*/
func Serve(appPort int) {
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/execute", ExecuteHandler)
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	_ = http.ListenAndServe(fmt.Sprintf(":%d", appPort), nil)
}
