
import $ from 'jquery';

class MainController {
    constructor() {
        this._fromDateInput = document.querySelector("#fromDate");
        this._toDateInput = document.querySelector("#toDate");
        this._fileInput = document.querySelector("#queryFile");
        this._executionTypeInput = document.querySelector("#executionType");
        this._localStorage = window.localStorage;
        this._results = document.querySelector("#executionResults");
        this._links = document.querySelector("#executionResults > li");
    }

    get from() {
        return this._fromDateInput;
    }

    get to() {
        return this._toDateInput;
    }

    get file() {
        return this._fileInput;
    }

    get type() {
        return this._executionTypeInput;
    }

    get count() {
        return this._results.querySelectorAll("li").length + 1;
    }

    get results() {
        return this._results;
    }

    getJiraMail() {
        return this._localStorage.getItem('jiraMail');
    }

    getJiraToken() {
        return this._localStorage.getItem('jiraToken');
    }

    showLoading() {
        $("#loading").modal({
            backdrop: "static", //remove ability to close modal with click
            keyboard: false, //remove option to close with keyboard
            show: true //Display loader!
        });
    }

    hideLoading() {
        setTimeout(() => $("#loading").modal("hide") , 1000);
    }

    addResult(result) {
        this.results.appendChild(result);
    }

    addError(error) {
        let counter = this.count;
        let li = document.createElement("li");
        li.classList = ["list-group-item list-group-item-danger"];
        let statusText = document.createTextNode(`Failed to execute query file: ${error}`);
        li.appendChild(statusText);

        this.addResult(li);
    }

    addLink(linkHref, text) {
        let linkText = document.createTextNode(`${text}`);
        let li = document.createElement("li");
        li.className = "list-group-item";
        let link = document.createElement("a");
        link.href = linkHref;
        link.download = `${text}`;
        link.appendChild(linkText);

        li.appendChild(link);

        this.addResult(li);
    }

    executeQuery(event) {
        this.showLoading();

        let url='/execute';

        var fd = new FormData();
        fd.append('queryFile', this.file.files[0]);
        fd.append('fromDate', this.from.value);
        fd.append('toDate', this.to.value);
        fd.append('mail', this.getJiraMail());
        fd.append('token', this.getJiraToken());

        var c = this;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                console.log((e.loaded / e.total) * 100);
            }
        };
        xhr.onload = function(e) {
            let counter = c.count;
            if (this.status == 200) {
                let response = this.response;
                var fullPath = document.getElementById('queryFile').value;

                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }

                c.addLink(`data:application/csv;base64,${btoa(response)}`, `${counter}-${filename}.csv`);

            } else {
                c.addError(this.responseText);
            }
            c.hideLoading();
        };

        xhr.send(fd);  // multipart/form-data
    }
}

export {MainController};
