
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../../static/style/style.css';

import $ from 'jquery';

import { MainController } from './MainController.js'

console.log("Running script...");

$(document).ready(function () {

    let controller = new MainController();

    document.querySelector('#executeQuery').onclick = event => controller.executeQuery(event);

    console.log(controller.file.value);
    console.log(controller.getJiraMail());

    var validarCredenciais = () => {
        var jiraEmail = controller.getJiraMail();
        var jiraToken = controller.getJiraToken();
        if ((jiraEmail != null) && (jiraToken != null)) {
            $("#btnDadosLoginJira").html("Edit login data");
        }
    }
    validarCredenciais();

    $('#executionForm').submit(function () {
        $("#loading").modal({
            backdrop: "static", //remove ability to close modal with click
            keyboard: false, //remove option to close with keyboard
            show: true //Display loader!
        });
        $('#mail').val(getJiraMail());
        $('#token').val(getJiraToken());
        return true; // return false to cancel form action
    });
    // Hide modal if "Okay" is pressed
    $('#loginModal .btn-primary').click(function () {
        $('#loginModal').modal('hide');
        localStorage.setItem('jiraMail', $('#jiraEmail').val());
        localStorage.setItem('jiraToken', $('#jiraToken').val());
        $("#btnDadosLoginJira").html("Edit login data");
    });

    $('#loginModal .btn-remove-data').click(function () {
        $('#loginModal').modal('hide');
        localStorage.clear();
        $("#btnDadosLoginJira").html("Add login data");
    });
});
